<?php

namespace App;

use Imagine\Gd\Font;
use Imagine\Gd\Imagine;
use Imagine\Image\ImageInterface;
use Imagine\Image\Palette\Color\RGB as ColorRGB;
use Imagine\Image\Palette\RGB;
use Imagine\Image\Point;

class ImageManipulation
{
    const TOP_CENTER = 0;
    const BOTTOM_CENTER = 1;

    private $fontSize = 50;
    /** @var RGB */
    private $palette;
    /** @var ColorRGB */
    private $white;
    /** @var ColorRGB */
    private $black;
    /** @var string */
    private $fontPath = __DIR__ . '/../fonts/Impact.ttf';
    /** @var ImageInterface */
    private $image;

    public function __construct(string $imagePath)
    {
        $this->image = (new Imagine())->open($imagePath);
        $this->palette = new RGB();
        $this->white = new ColorRGB($this->palette, [255, 255, 255], 100);
        $this->black = new ColorRGB($this->palette, [0, 0, 0], 70);
    }

    public function setTtf(string $fontPath)
    {
        $this->$fontPath = $fontPath;
    }

    public function output()
    {
        $this->image->show('jpeg');
    }

    public function write(string $text, string $position)
    {
        $textDimensions = $this->getFont($this->white)->box($text);
        $padding = 20;

        switch ($position) {
            case self::TOP_CENTER:
                $point = new Point($this->image->getSize()->getWidth() / 2 - $textDimensions->getWidth() / 2, $padding);
                break;
            case self::BOTTOM_CENTER:
                $point = new Point(
                    $this->image->getSize()->getWidth() / 2 - $textDimensions->getWidth() / 2,
                    $this->image->getSize()->getHeight() - $padding - $textDimensions->getHeight()
                );
                break;
            default:
                $point = new Point(0, 0);
        }
        $pointShadow = new Point($point->getX() + 3, $point->getY() + 3);
        $this->image->draw()->text($text, $this->getFont($this->black), $pointShadow);
        $this->image->draw()->text($text, $this->getFont($this->white), $point);
    }

    private function getFont($color)
    {
        return new Font($this->fontPath, $this->fontSize, $color);
    }

    /**
     * @param int $fontSize
     * @return ImageManipulation
     */
    public function setFontSize(int $fontSize): ImageManipulation
    {
        $this->fontSize = $fontSize;
        return $this;
    }
}
