<?php

namespace App;

class Header
{
    /** @var int */
    private $timestamp = 123456;

    public function enableCaching(string $hash): self
    {
        $etag = sha1(date('mY') . $hash);
        $tsString = gmdate('D, d M Y H:i:s ', $this->timestamp) . 'GMT';

        header('Cache-Control: "max-age=290304000, public"');
        header("Last-Modified: {$tsString}");
        header("ETag: \"{$etag}\"");

        return $this;
    }

    public function setContentType(string $contentType = 'image/jpeg'): self
    {
        header("Content-type: $contentType");
        return $this;
    }

    /**
     * @param int $timestamp
     */
    public function setTimestamp(int $timestamp): void
    {
        $this->timestamp = $timestamp;
    }

}
