<?php

namespace App;

class Domains
{
    const IST_FUER_MICH_ABFALL = [
        'upper' => 'BIO',
        'lower' => 'IST FÜR MICH ABFALL',
    ];

    const SIND_FUER_MICH_ABFALL = [
        'upper' => 'WURSTVITAMINE',
        'lower' => 'SIND FÜR MICH ABFALL',
    ];
}
