<?php
preg_match('@^(.*?)\..*@', $_SERVER['HTTP_HOST'], $matches);
$name = strtoupper($matches[1]);
$name = substr(str_replace('-', ' ', $name), 0, 30);

preg_match('@.*\.(ist|sind)fuermichabfall\.de$@',$_SERVER['HTTP_HOST'], $matches);

$bla = strtoupper($matches[1]);
$white = new \ImagickPixel('#FFF');
$black = new \ImagickPixel('#300');
$strokeWith = 2;
$etag = sha1(date('dmY'));
$timestamp = 123456;
$tsstring = gmdate('D, d M Y H:i:s ', $timestamp) . 'GMT';

$draw = new \ImagickDraw();
$draw->setFont(__DIR__ . '/Impact.ttf');
$draw->setFontSize(70);

$outputImage = new \Imagick(__DIR__ . '/nadine.jpg');

$draw->setFillColor($white);
$draw->setGravity(\Imagick::GRAVITY_NORTH);
$draw->setStrokeColor($black);
$draw->setStrokeWidth($strokeWith);
$draw->setStrokeAntialias(true);
$draw->setTextAntialias(true);
$outputImage->annotateImage($draw, 0, 5, 0, $name);

$draw->setFillColor($white);
 $draw->setGravity(\Imagick::GRAVITY_SOUTH);
$draw->setStrokeColor($black);
$draw->setStrokeWidth($strokeWith);
$draw->setStrokeAntialias(true);
$draw->setTextAntialias(true);
$outputImage->annotateImage($draw, 0, 5, 0, $bla . ' FUER MICH ABFALL');

$outputImage->setFormat('jpg');


header("Content-type: image/jpeg");
header('Cache-Control: "max-age=290304000, public"');
header("Last-Modified: $tsstring");
header("ETag: \"{$etag}\"");
echo $outputImage;
$outputImage->clear();

