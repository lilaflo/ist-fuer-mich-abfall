<?php

use App\Domains;
use App\Header;
use App\ImageManipulation;
use TrueBV\Punycode;

include __DIR__ . '/vendor/autoload.php';

// Read text from sub domain
$domainArray = explode('.', $_SERVER['HTTP_HOST']);
list($tld, $domain, $subDomain) = array_reverse($domainArray);
$config = Domains::IST_FUER_MICH_ABFALL;

if ($domain === 'sindfuermichabfall') {
    $config = Domains::SIND_FUER_MICH_ABFALL;
}

$upper = $config['upper'];
$lower = $config['lower'];

if ($subDomain) {
    $Punycode = new Punycode();
    $upper = strtoupper(substr(str_replace('-', ' ', $Punycode->decode($subDomain)), 0, 30));
}

$header = new Header();
$header->enableCaching($upper . $lower);
$header->setContentType();

$image = new ImageManipulation(__DIR__ . '/images/nadine.jpg');
$image->setFontSize(60 - strlen($upper));
$image->write($upper, ImageManipulation::TOP_CENTER);
$image->setFontSize(60);
$image->write($lower, ImageManipulation::BOTTOM_CENTER);

$image->output();
